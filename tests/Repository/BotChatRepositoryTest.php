<?php

namespace App\Tests\Repository;

use App\Entity\BotChat;
use App\Enum\Trend;
use App\Repository\BotChatRepository;
use App\Tests\AbstractRepositoryTestCase;

class BotChatRepositoryTest extends AbstractRepositoryTestCase
{
    private BotChatRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->getRepositoryForEntity(BotChat::class);
    }

    public function testFindOneByTelegramId()
    {
        $botChat = $this->createBotChat(1234567, 'john_doe');

        $this->em->persist($botChat);
        $this->em->flush();

        $this->assertInstanceOf(BotChat::class, $this->repository->findOneByTelegramId(1234567));
    }

    public function testDidntFindOneByTelegramId()
    {
        $botChat = $this->createBotChat(1234568, 'john_doe');

        $this->em->persist($botChat);
        $this->em->flush();

        $this->assertNull($this->repository->findOneByTelegramId(1234569));
    }

    private function createBotChat(int $telegramId, string $username): BotChat
    {
        return (new BotChat())
            ->setExChatId($telegramId)
            ->setTelegramUsername($username);
    }
}

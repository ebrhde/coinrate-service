<?php

namespace App\Tests\Repository;

use App\Entity\Coin;
use App\Enum\Trend;
use App\Repository\CoinRepository;
use App\Tests\AbstractRepositoryTestCase;

class CoinRepositoryTest extends AbstractRepositoryTestCase
{
    private CoinRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->getRepositoryForEntity(Coin::class);
    }

    public function testFindOneByTitle()
    {
        for ($i = 0; $i <= 3; ++$i) {
            $coin = $this->createCoin('Coin'.$i, 'CN'.$i);
            $this->em->persist($coin);
        }

        $this->em->flush();

        $this->assertInstanceOf(Coin::class, $this->repository->findOneByTitleOrAbbreviation('Coin2'));
    }

    public function testFindOneByAbbreviation()
    {
        for ($i = 4; $i <= 6; ++$i) {
            $coin = $this->createCoin('Coin'.$i, 'CN'.$i);
            $this->em->persist($coin);
        }

        $this->em->flush();

        $this->assertInstanceOf(Coin::class, $this->repository->findOneByTitleOrAbbreviation('CN5'));
    }

    public function testCoinNotFound()
    {
        for ($i = 7; $i <= 9; ++$i) {
            $coin = $this->createCoin('Coin'.$i, 'CN'.$i);
            $this->em->persist($coin);
        }

        $this->em->flush();

        $this->assertNull($this->repository->findOneByTitleOrAbbreviation('Litecoin'));
    }

    private function createCoin(string $title, string $abbreviation): Coin
    {
        return (new Coin())
            ->setTitle($title)
            ->setAbbreviation($abbreviation)
            ->setPrice(0.00)
            ->setDailyPriceChange(0.00)
            ->setTrend(Trend::None);
    }
}

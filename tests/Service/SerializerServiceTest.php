<?php

namespace App\Tests\Service;
use PHPUnit\Framework\TestCase;
use App\Service\SerializerService;
use App\Exception\JsonInvalidException;
use App\Exception\ClassNotFoundException;

class SerializerServiceTest extends TestCase
{
    private SerializerService $serializerService;

    protected function setUp(): void
    {
        $this->serializerService = new SerializerService();
    }

    public function testDeserializeArray(): void
    {
        $data = '[{"title":"Bitcoin","abbreviation":"BTC"},{"title":"Litecoin","abbreviation":"LTC"}]';
        $objectClass = 'App\Entity\Coin';

        $result = $this->serializerService->deserializeArray($data, $objectClass);

        $this->assertIsArray($result);
        $this->assertCount(2, $result);

        $this->assertInstanceOf($objectClass, $result[0]);
        $this->assertInstanceOf($objectClass, $result[1]);
    }

    public function testDeserializeArrayWithInvalidData(): void
    {
        $this->expectException(JsonInvalidException::class);

        $data = 'invalid_json_data';
        $objectClass = 'App\Entity\Coin';

        $this->serializerService->deserializeArray($data, $objectClass);
    }

    public function testDeserializeArrayWithNonExistentClass(): void
    {
        $this->expectException(ClassNotFoundException::class);

        $data = '[{"name":"John","age":30}]';
        $objectClass = 'App\Entity\User';

        $this->serializerService->deserializeArray($data, $objectClass);
    }

    public function testDenormalizeOne(): void
    {
        $data = ['title' => 'Bitcoin', 'abbreviation' => "BTC"];
        $objectClass = 'App\Entity\Coin';

        $result = $this->serializerService->denormalizeOne($data, $objectClass);

        $this->assertInstanceOf($objectClass, $result);
        $this->assertEquals('Bitcoin', $result->getTitle());
        $this->assertEquals('BTC', $result->getAbbreviation());
    }

    public function testDecode(): void
    {
        $data = '{"name":"John","age":30}';

        $result = $this->serializerService->decode($data);

        $this->assertIsArray($result);
        $this->assertEquals(['name' => 'John', 'age' => 30], $result);
    }
}
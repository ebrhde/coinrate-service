<?php

namespace App\Tests\Service;

use App\Enum\Trend;
use App\Service\HandleTelegramUpdateService;
use App\Entity\Coin;
use App\Repository\CoinRepository;
use App\Repository\BotChatRepository;
use App\Entity\BotChat;
use App\Api\TelegramApi;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class HandleTelegramUpdateServiceTest extends TestCase
{
    private CoinRepository $coinRepositoryMock;
    private BotChatRepository $chatRepositoryMock;
    private EntityManagerInterface $managerMock;
    private TelegramApi $apiMock;
    private HandleTelegramUpdateService $service;

    protected function setUp(): void
    {
        $this->coinRepositoryMock = $this->createMock(CoinRepository::class);
        $this->chatRepositoryMock = $this->createMock(BotChatRepository::class);
        $this->managerMock = $this->createMock(EntityManagerInterface::class);
        $this->apiMock = $this->createMock(TelegramApi::class);

        $this->service = new HandleTelegramUpdateService(
            $this->coinRepositoryMock,
            $this->chatRepositoryMock,
            $this->managerMock,
            $this->apiMock
        );
    }

    public function testHandleStartCommand()
    {
        $update = [
            'update_id' => 123,
            'message' => [
                'date' => time(),
                'chat' => [
                    'id' => 456,
                ],
                'from' => [
                    'username' => 'john_doe',
                ],
                'text' => '/start',
            ],
        ];

        $username = $update['message']['from']['username'];

        $this->chatRepositoryMock->expects($this->once())
            ->method('findOneByTelegramId')
            ->with($update['message']['chat']['id'])
            ->willReturn(null);

        $this->apiMock->expects($this->once())
            ->method('sendMessage')
            ->with($update['message']['chat']['id'], $this->isType('string'))
            ->willReturn(true);

        $response = $this->service->handle($update);

        $this->assertEquals("Handled request from new user: $username", $response);
    }

    public function testHandleRegularMessage()
    {
        $update = [
            'update_id' => 123,
            'message' => [
                'date' => time(),
                'chat' => [
                    'id' => 456,
                ],
                'from' => [
                    'username' => 'john_doe',
                ],
                'text' => 'BTC',
            ],
        ];

        $coin = new Coin();
        $coin->setAbbreviation('BTC');
        $coin->setTitle('Bitcoin');
        $coin->setTrend(Trend::Up);
        $coin->setPrice(40000);
        $coin->setDailyPriceChange(2.5);
        $coin->setCreatedAtValue();
        $coin->setUpdatedAtValue();

        $this->coinRepositoryMock->expects($this->once())
            ->method('findOneByTitleOrAbbreviation')
            ->with($update['message']['text'])
            ->willReturn($coin);

        $this->apiMock->expects($this->once())
            ->method('sendMessage')
            ->with($update['message']['chat']['id'], $this->isType('string'))
            ->willReturn(true);

        $this->managerMock->expects($this->exactly(2))
            ->method('persist');

        $this->managerMock->expects($this->once())
            ->method('flush');

        $response = $this->service->handle($update);

        $expectedResponse = "<b>BTC | Bitcoin</b>\n"
            ."Price on ".date('H:i d.m', $coin->getUpdated()->getTimestamp()).": \n"
            .$coin->getPrice()."$ (↗ ".$coin->getDailyPriceChange()."%)";

        $this->assertEquals("Handled request from user: john_doe.\n"
            ."Message 'BTC' got a reply '$expectedResponse'", $response);
    }
}
<?php

namespace App\Tests\Job;

use App\Api\CMCApi;
use App\Entity\Coin;
use App\Exception\JsonInvalidException;
use App\Job\PopulateCoinbaseJob;
use App\Repository\CoinRepository;
use App\Service\SerializerService;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Test\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class PopulateCoinbaseJobTest extends TestCase
{
    private CommandTester $commandTester;

    private CMCApi $apiMock;
    private CoinRepository $repositoryMock;
    private SerializerService $serviceMock;
    private EntityManagerInterface $managerMock;
    private LoggerInterface $loggerMock;

    protected function setUp(): void
    {
        $this->apiMock = $this->createPartialMock(CMCApi::class, ['getMapData']);
        $this->repositoryMock = $this->createPartialMock(CoinRepository::class, ['getCount']);
        $this->serviceMock = $this->createPartialMock(SerializerService::class, ['deserializeArray']);
        $this->managerMock = $this->createMock(EntityManagerInterface::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);

        $application = new Application();
        $application->add(new PopulateCoinbaseJob($this->apiMock, $this->managerMock, $this->repositoryMock, $this->serviceMock, $this->loggerMock));

        $command = $application->find('app:populate-coinbase');
        $this->commandTester = new CommandTester($command);
    }


    public function testCoinbaseAlreadyPopulated()
    {
        $this->repositoryMock->expects($this->once())->method('getCount')->willReturn(1);

        $this->assertEquals(Command::FAILURE, $this->commandTester->execute([]));
        $this->assertStringContainsString(PopulateCoinbaseJob::ALREADY_POPULATED_MESSAGE, $this->commandTester->getDisplay());
    }

    public function testApiNotAvailable()
    {
        $this->repositoryMock->expects($this->once())->method('getCount')->willReturn(0);
        $this->apiMock->expects($this->once())->method('getMapData')->willReturn(null);

        $this->assertEquals(Command::FAILURE, $this->commandTester->execute([]));
        $this->assertStringContainsString(PopulateCoinbaseJob::API_NOT_AVAILABLE_MESSAGE, $this->commandTester->getDisplay());
    }

    public function testServiceThrowsException()
    {
        $this->repositoryMock->expects($this->once())->method('getCount')->willReturn(0);
        $this->apiMock->expects($this->once())->method('getMapData')->willReturn('111');
        $this->serviceMock->expects($this->once())
            ->method('deserializeArray')
            ->with('111', 'App\\Entity\\Coin', '[data]')
            ->willThrowException(new JsonInvalidException());

        $this->assertEquals(Command::FAILURE, $this->commandTester->execute([]));
        $this->assertStringContainsString(PopulateCoinbaseJob::FAIL_MESSAGE, $this->commandTester->getDisplay());
    }

    public function testCommandSuccess() {
        $this->repositoryMock->expects($this->once())->method('getCount')->willReturn(0);
        $this->apiMock->expects($this->once())->method('getMapData')->willReturn($this->getTestApiResponse());

        $coin = $this->createMock(Coin::class);
        $this->serviceMock->expects($this->once())
            ->method('deserializeArray')
            ->with($this->getTestApiResponse(), 'App\\Entity\\Coin', '[data]')
            ->willReturn([$coin]);

        $this->managerMock->expects($this->once())
            ->method('persist')
            ->with($coin);

        $this->managerMock->expects($this->once())
            ->method('flush');

        $this->assertEquals(Command::SUCCESS, $this->commandTester->execute([]));
        $this->assertStringContainsString(PopulateCoinbaseJob::SUCCESS_MESSAGE, $this->commandTester->getDisplay());
    }

    private function getTestApiResponse(): string
    {
        return '"data": [
    {
      "id": 1,
      "rank": 1,
      "name": "Bitcoin",
      "symbol": "BTC",
      "slug": "bitcoin",
      "is_active": 1,
      "first_historical_data": "2010-07-13T00:00:00.000Z",
      "last_historical_data": "2023-06-04T09:49:00.000Z",
      "platform": null
    },
    {
      "id": 2,
      "rank": 12,
      "name": "Litecoin",
      "symbol": "LTC",
      "slug": "litecoin",
      "is_active": 1,
      "first_historical_data": "2013-04-28T18:47:22.000Z",
      "last_historical_data": "2023-06-04T09:49:00.000Z",
      "platform": null
    }
  ]';
    }
}

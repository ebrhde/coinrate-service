<?php

namespace App\Tests\Job;

use App\Entity\Coin;
use App\Exception\JsonInvalidException;
use App\Job\UpdateRatesJob;
use App\Repository\CoinRepository;
use App\Api\CMCApi;
use App\Service\SerializerService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Console\Application;

class UpdateRatesJobTest extends TestCase
{
    private $api;
    private $repository;
    private $service;
    private $manager;
    private $logger;

    protected function setUp(): void
    {
        $this->api = $this->createMock(CMCApi::class);
        $this->repository = $this->createPartialMock(CoinRepository::class, ['findOneByTitleOrAbbreviation']);
        $this->service = $this->createMock(SerializerService::class);
        $this->manager = $this->createMock(EntityManagerInterface::class);
        $this->logger= $this->createMock(LoggerInterface::class);

        $application = new Application();
        $application->add(new UpdateRatesJob($this->api, $this->repository, $this->service, $this->manager, $this->logger));

        $command = $application->find('app:update-rates');
        $this->commandTester = new CommandTester($command);
    }

    public function testFailure()
    {
        $this->api->expects($this->once())
            ->method('getListingData')
            ->willReturn(null);

        $this->assertEquals(Command::FAILURE, $this->commandTester->execute([]));
        $this->assertStringContainsString('Abort: no coin data or API is not available', $this->commandTester->getDisplay());
    }

    public function testSuccessWhenCoinFound()
    {
        $this->api->expects($this->once())
            ->method('getListingData')
            ->willReturn($this->getTestApiResponse());

        $coinData = ['name' => 'BTC', 'quote' => ['USD' => ['price' => 50000.87, 'percent_change_24h' => 1.05]]];
        $coinObject = $this->createMock(Coin::class);

        $this->service->expects($this->once())
            ->method('decode')
            ->with($this->getTestApiResponse())
            ->willReturn(['data' => [$coinData]]);

        $this->repository->expects($this->once())
            ->method('findOneByTitleOrAbbreviation')
            ->with($coinData['name'])
            ->willReturn($coinObject);

        $this->manager->expects($this->once())
            ->method('persist')
            ->with($coinObject);

        $this->manager->expects($this->once())
            ->method('flush');

        $this->assertEquals(Command::SUCCESS, $this->commandTester->execute([]));
        $this->assertStringContainsString(UpdateRatesJob::SUCCESS_MESSAGE, $this->commandTester->getDisplay());
    }

    public function testSuccessWhenCoinNotFound()
    {
        $this->api->expects($this->once())
            ->method('getListingData')
            ->willReturn($this->getTestApiResponse());

        $coinData = ['name' => 'BTC', 'quote' => ['USD' => ['price' => 50000.87, 'percent_change_24h' => 1.05]]];
        $coinObject = $this->createMock(Coin::class);

        $this->service->expects($this->once())
            ->method('decode')
            ->with($this->getTestApiResponse())
            ->willReturn(['data' => [$coinData]]);

        $this->repository->expects($this->once())
            ->method('findOneByTitleOrAbbreviation')
            ->with($coinData['name'])
            ->willReturn(null);

        $this->service->expects($this->once())
            ->method('denormalizeOne')
            ->with($coinData, 'App\\Entity\\Coin')
            ->willReturn($coinObject);

        $this->manager->expects($this->once())
            ->method('persist')
            ->with($coinObject);

        $this->manager->expects($this->once())
            ->method('flush');

        $this->assertEquals(Command::SUCCESS, $this->commandTester->execute([]));
        $this->assertStringContainsString(UpdateRatesJob::SUCCESS_MESSAGE, $this->commandTester->getDisplay());
    }

    private function getTestApiResponse(): string
    {
        return '"data": [
    {
      "id": 1,
      "name": "Bitcoin",
      "symbol": "BTC",
      "slug": "bitcoin",
      "num_market_pairs": 10241,
      "date_added": "2010-07-13T00:00:00.000Z",
      "tags": [
        "mineable",
        "pow",
        "sha-256",
        "store-of-value",
        "state-channel",
        "coinbase-ventures-portfolio",
        "three-arrows-capital-portfolio",
        "polychain-capital-portfolio",
        "binance-labs-portfolio",
        "blockchain-capital-portfolio",
        "boostvc-portfolio",
        "cms-holdings-portfolio",
        "dcg-portfolio",
        "dragonfly-capital-portfolio",
        "electric-capital-portfolio",
        "fabric-ventures-portfolio",
        "framework-ventures-portfolio",
        "galaxy-digital-portfolio",
        "huobi-capital-portfolio",
        "alameda-research-portfolio",
        "a16z-portfolio",
        "1confirmation-portfolio",
        "winklevoss-capital-portfolio",
        "usv-portfolio",
        "placeholder-ventures-portfolio",
        "pantera-capital-portfolio",
        "multicoin-capital-portfolio",
        "paradigm-portfolio",
        "bitcoin-ecosystem"
      ],
      "max_supply": 21000000,
      "circulating_supply": 19392468,
      "total_supply": 19392468,
      "infinite_supply": false,
      "platform": null,
      "cmc_rank": 1,
      "self_reported_circulating_supply": null,
      "self_reported_market_cap": null,
      "tvl_ratio": null,
      "last_updated": "2023-06-04T11:25:00.000Z",
      "quote": {
        "USD": {
          "price": 27233.494246328788,
          "volume_24h": 8712417105.80676,
          "volume_change_24h": -23.4257,
          "percent_change_1h": 0.08392798,
          "percent_change_24h": 0.27389229,
          "percent_change_7d": 0.04459832,
          "percent_change_30d": -6.50634464,
          "percent_change_60d": -4.57741329,
          "percent_change_90d": 21.55673698,
          "market_cap": 528124665700.1151,
          "market_cap_dominance": 45.9202,
          "fully_diluted_market_cap": 571903379172.9,
          "tvl": null,
          "last_updated": "2023-06-04T11:25:00.000Z"
        }
      }
    },
    {
      "id": 1027,
      "name": "Ethereum",
      "symbol": "ETH",
      "slug": "ethereum",
      "num_market_pairs": 6967,
      "date_added": "2015-08-07T00:00:00.000Z",
      "tags": [
        "pos",
        "smart-contracts",
        "ethereum-ecosystem",
        "coinbase-ventures-portfolio",
        "three-arrows-capital-portfolio",
        "polychain-capital-portfolio",
        "binance-labs-portfolio",
        "blockchain-capital-portfolio",
        "boostvc-portfolio",
        "cms-holdings-portfolio",
        "dcg-portfolio",
        "dragonfly-capital-portfolio",
        "electric-capital-portfolio",
        "fabric-ventures-portfolio",
        "framework-ventures-portfolio",
        "hashkey-capital-portfolio",
        "kenetic-capital-portfolio",
        "huobi-capital-portfolio",
        "alameda-research-portfolio",
        "a16z-portfolio",
        "1confirmation-portfolio",
        "winklevoss-capital-portfolio",
        "usv-portfolio",
        "placeholder-ventures-portfolio",
        "pantera-capital-portfolio",
        "multicoin-capital-portfolio",
        "paradigm-portfolio",
        "injective-ecosystem",
        "layer-1"
      ],
      "max_supply": null,
      "circulating_supply": 120236126.76773931,
      "total_supply": 120236126.76773931,
      "infinite_supply": true,
      "platform": null,
      "cmc_rank": 2,
      "self_reported_circulating_supply": null,
      "self_reported_market_cap": null,
      "tvl_ratio": null,
      "last_updated": "2023-06-04T11:25:00.000Z",
      "quote": {
        "USD": {
          "price": 1905.272645331915,
          "volume_24h": 3398227567.8702064,
          "volume_change_24h": -28.7052,
          "percent_change_1h": 0.01526049,
          "percent_change_24h": -0.01234647,
          "percent_change_7d": 3.24796454,
          "percent_change_30d": 0.02990534,
          "percent_change_60d": -0.46437969,
          "percent_change_90d": 21.62855808,
          "market_cap": 229082603311.23416,
          "market_cap_dominance": 19.9207,
          "fully_diluted_market_cap": 229082603311.23,
          "tvl": null,
          "last_updated": "2023-06-04T11:25:00.000Z"
        }
      }
    }
  ]';
    }
}

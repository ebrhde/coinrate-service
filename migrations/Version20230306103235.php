<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230306103235 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE bot_chat_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bot_chat_update_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bot_chat (id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ex_chat_id INT NOT NULL, telegram_username VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN bot_chat.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE bot_chat_update (id INT NOT NULL, chat_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ex_update_id BIGINT NOT NULL, ex_created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, message VARCHAR(255) NOT NULL, response VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_47FEAC901A9A7125 ON bot_chat_update (chat_id)');
        $this->addSql('COMMENT ON COLUMN bot_chat_update.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN bot_chat_update.ex_created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE bot_chat_update ADD CONSTRAINT FK_47FEAC901A9A7125 FOREIGN KEY (chat_id) REFERENCES bot_chat (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE bot_chat_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bot_chat_update_id_seq CASCADE');
        $this->addSql('ALTER TABLE bot_chat_update DROP CONSTRAINT FK_47FEAC901A9A7125');
        $this->addSql('DROP TABLE bot_chat');
        $this->addSql('DROP TABLE bot_chat_update');
    }
}

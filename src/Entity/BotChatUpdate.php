<?php

namespace App\Entity;

use App\Repository\BotChatUpdateRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BotChatUpdateRepository::class)]
#[ORM\HasLifecycleCallbacks]
class BotChatUpdate
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(type: Types::BIGINT)]
    private ?int $ex_update_id = null;

    #[ORM\ManyToOne(targetEntity: BotChat::class, inversedBy: 'botChatUpdates')]
    private ?BotChat $chat = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeImmutable $ex_created_at = null;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $message = null;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $response = null;

    #[ORM\PrePersist]
    public function setCreatedAtValue(): void
    {
        $this->created_at = new \DateTimeImmutable('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function getExUpdateId(): ?int
    {
        return $this->ex_update_id;
    }

    public function setExUpdateId(int $ex_update_id): self
    {
        $this->ex_update_id = $ex_update_id;

        return $this;
    }

    public function getChat(): ?BotChat
    {
        return $this->chat;
    }

    public function setChat(?BotChat $chat): self
    {
        $this->chat = $chat;

        return $this;
    }

    public function getExCreatedAt(): ?\DateTimeImmutable
    {
        return $this->ex_created_at;
    }

    public function setExCreatedAt(\DateTimeImmutable $ex_created_at): self
    {
        $this->ex_created_at = $ex_created_at;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(string $response): self
    {
        $this->response = $response;

        return $this;
    }
}

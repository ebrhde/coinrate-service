<?php

namespace App\Entity;

use App\Enum\Trend;
use App\Repository\CoinRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Annotation\SerializedPath;

#[ORM\Entity(repositoryClass: CoinRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Coin
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private ?\DateTimeImmutable $updated_at = null;

    #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
    #[SerializedName('name')]
    private ?string $title = null;

    #[ORM\Column(type: Types::STRING, length: 10)]
    #[SerializedName('symbol')]
    private ?string $abbreviation = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    private ?string $price = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    private ?string $daily_price_change = null;

    #[ORM\Column(type: Types::STRING, length: 4, enumType: Trend::class)]
    private ?Trend $trend = null;

    public function __construct()
    {
        self::setPrice('0.00');
        self::setDailyPriceChange('0.00');
        self::setTrend(Trend::None);
    }

    #[ORM\PrePersist]
    public function setCreatedAtValue(): void
    {
        $this->created_at = new \DateTimeImmutable('now');
        $this->updated_at = new \DateTimeImmutable('now');
    }

    #[ORM\PreUpdate]
    public function setUpdatedAtValue(): void
    {
        $this->updated_at = new \DateTimeImmutable('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function getUpdated(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDailyPriceChange(): ?string
    {
        return $this->daily_price_change;
    }

    public function setDailyPriceChange(string $daily_price_change): self
    {
        $this->daily_price_change = $daily_price_change;

        return $this;
    }

    public function getTrend(): ?Trend
    {
        return $this->trend;
    }

    public function setTrend(Trend $trend): self
    {
        $this->trend = $trend;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\BotChatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BotChatRepository::class)]
#[ORM\HasLifecycleCallbacks]
class BotChat
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column]
    private ?int $ex_chat_id = null;

    #[ORM\Column(length: 255)]
    private ?string $telegram_username = null;

    #[ORM\OneToMany(mappedBy: 'botChat', targetEntity: BotChatUpdate::class)]
    private $updates;

    public function __construct()
    {
        $this->updates = new ArrayCollection();
    }

    #[ORM\PrePersist]
    public function setCreatedAtValue(): void
    {
        $this->created_at = new \DateTimeImmutable('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function getExChatId(): ?int
    {
        return $this->ex_chat_id;
    }

    public function setExChatId(int $ex_chat_id): self
    {
        $this->ex_chat_id = $ex_chat_id;

        return $this;
    }

    public function getTelegramUsername(): ?string
    {
        return $this->telegram_username;
    }

    public function setTelegramUsername(string $telegram_username): self
    {
        $this->telegram_username = $telegram_username;

        return $this;
    }

    /**
     * @return Collection|BotChatUpdate[]
     */
    public function getUpdates(): Collection
    {
        return $this->updates;
    }
}

<?php

namespace App\Exception;

class NoDataException extends \RuntimeException
{
    public function __construct(string $path)
    {
        parent::__construct("No data found on given path: $path.");
    }
}

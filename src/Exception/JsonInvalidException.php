<?php

namespace App\Exception;

class JsonInvalidException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Given JSON is invalid.');
    }
}

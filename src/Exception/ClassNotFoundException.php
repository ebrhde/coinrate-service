<?php

namespace App\Exception;

class ClassNotFoundException extends \RuntimeException
{
    public function __construct(string $className)
    {
        parent::__construct("Class of name '$className' not found.");
    }
}

<?php

namespace App\Exception;

class MessageNotSentException extends \RuntimeException
{
    public function __construct(string $message)
    {
        parent::__construct("Message with text '$message' wasn't sent");
    }
}

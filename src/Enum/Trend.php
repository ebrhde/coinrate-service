<?php

namespace App\Enum;

enum Trend: string
{
    case Up = 'up';
    case Down = 'down';
    case None = 'none';
}

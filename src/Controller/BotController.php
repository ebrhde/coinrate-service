<?php

namespace App\Controller;

use App\Service\HandleTelegramUpdateService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class BotController extends AbstractController
{
    public function __construct(private HandleTelegramUpdateService $service)
    {
    }

    #[Route('/', name: 'bot_update', methods: ['POST'])]
    public function index(Request $request): Response
    {
        if ($request->getContentTypeFormat() != 'json' || !$request->getContent()) {
            return $this->json(['status' => 'error']);
        }

        $data = json_decode($request->getContent(), true);

        if($data && $this->service->handle($data)) {
            return $this->json(['status' => 'ok']);
        }

        return $this->json(['status' => 'error']);
    }
}
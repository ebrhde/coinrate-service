<?php

namespace App\Job;

use App\Api\CoinbaseApiInterface;
use App\Enum\Trend;
use App\Repository\CoinRepository;
use App\Service\SerializerService;
use App\Trait\DuplicateRemover;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:update-rates',
    description: 'Updates exchange rates (price in USD and change of price for 24h in %',
    aliases: ['app:update-rates'],
    hidden: false
)]
class UpdateRatesJob extends Command
{
    use DuplicateRemover;

    public const FAIL_MESSAGE = 'Coins exchanging rates update failed. See logs for details';
    public const SUCCESS_MESSAGE = 'Coins exchanging rates updated!';

    public function __construct(
        private CoinbaseApiInterface $api,
        private CoinRepository $repository,
        private SerializerService $service,
        private EntityManagerInterface $manager,
        private LoggerInterface $consoleCommandLogger
    ) {
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Updating coins exchanging rates');

        $apiData = $this->api->getListingData();

        if (!$apiData) {
            $this->consoleCommandLogger->notice('Tried to update exchanging coin rates. No coin data or API is not available');
            $output->writeln('Abort: no coin data or API is not available');

            return Command::FAILURE;
        }

        try {
            $coins = $this->service->decode($apiData)['data'];
            $coins = $this->removeArrayDuplicates($coins, 'symbol');

            foreach ($coins as $coinData) {
                if (!$coinObject = $this->repository->findOneByAbbreviation($coinData['symbol'])) {
                    $coinObject = $this->service->denormalizeOne($coinData, 'App\\Entity\\Coin');
                }

                $this->updateRate($coinData['quote']['USD'], $coinObject);
            }

            $this->manager->flush();
        } catch (\Throwable $t) {
            $this->consoleCommandLogger->error(message: $t->getMessage().' '.$t->getFile().':'.$t->getLine());
            $output->writeln(self::FAIL_MESSAGE);

            return Command::FAILURE;
        }

        $output->writeln(self::SUCCESS_MESSAGE);

        return Command::SUCCESS;
    }

    private function updateRate($data, $coinObject)
    {
        $price = $data['price'];
        $dailyChange = $data['percent_change_24h'];
        $trend = Trend::None;

        if (0 !== $dailyChange) {
            $formattedPrice = $this->getFormattedFloat($price);
            $formattedDailyChange = $this->getFormattedFloat($dailyChange);
            $trend = $dailyChange > 0 ? Trend::Up : Trend::Down;

            $coinObject->setPrice($formattedPrice);
            $coinObject->setDailyPriceChange($formattedDailyChange);
            $coinObject->setTrend($trend);

            $this->manager->persist($coinObject);
        }
    }

    private function getFormattedFloat(float $float): string
    {
        $float = abs($float);
        $explodedFloat = explode('.', $float);

        $decimalPart = isset($explodedFloat[1]) ? substr($explodedFloat[1], 0, 2) : '00';

        return $explodedFloat[0].'.'.$decimalPart;
    }
}

<?php

namespace App\Job;

use App\Api\TelegramApi;
use App\Repository\BotChatUpdateRepository;
use App\Service\HandleTelegramUpdateService;
use JetBrains\PhpStorm\NoReturn;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:watch-telegram',
    description: 'Handles requests from users in telegram bot',
    aliases: ['app:handle-telegram'],
    hidden: false
)]
class WatchTelegramUpdates extends Command
{
    public function __construct(
        private TelegramApi $api,
        private BotChatUpdateRepository $repository,
        private HandleTelegramUpdateService $service,
        private LoggerInterface $consoleCommandLogger
    ) {
        parent::__construct();
    }

    #[NoReturn]
 public function execute(InputInterface $input, OutputInterface $output): void
 {
     while (1) {
         $lastUpdateId = 0;

         if ($lastUpdate = $this->repository->getLastRecord()) {
             $lastUpdateId = $lastUpdate->getExUpdateId() + 1;
         }
         $updates = $this->api->getUpdates($lastUpdateId);

         if (is_array($updates) && !empty($updates)) {
             foreach ($updates as $update) {
                 if (isset($update['message'])) {
                     try {
                         $output->writeln($this->service->handle($update));
                     } catch (\Throwable $t) {
//                         $this->consoleCommandLogger->error($t->getMessage().' '.$t->getFile().':'.$t->getLine()); // temporarily off to prevent log file becoming too heavy
                     }
                 }
             }
         }

         sleep(5);
     }
 }
}

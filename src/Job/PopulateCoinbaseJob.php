<?php

namespace App\Job;

use App\Api\CoinbaseApiInterface;
use App\Repository\CoinRepository;
use App\Service\SerializerService;
use App\Trait\DuplicateRemover;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


#[AsCommand(
    name: 'app:populate-coinbase',
    description: 'Populates coins table with entities (titles and symbols)',
    aliases: ['app:populate-coinbase'],
    hidden: false
)]
class PopulateCoinbaseJob extends \Symfony\Component\Console\Command\Command
{
    use DuplicateRemover;

    public const FAIL_MESSAGE = 'Coinbase population failed. See logs for details.';
    public const SUCCESS_MESSAGE = 'Coin table successfully populated.';
    public const ALREADY_POPULATED_MESSAGE = 'Coinbase is already populated.';
    public const API_NOT_AVAILABLE_MESSAGE = 'Coinbase population failed. No coin data or API is not available';

    public function __construct(
        private CoinbaseApiInterface $api,
        private EntityManagerInterface $manager,
        private CoinRepository $repository,
        private SerializerService $service,
        private LoggerInterface $consoleCommandLogger
    ) {
        parent::__construct();
    }

    /**
     * @throws \Throwable
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Populating coinbase');

        if (!$this->repository->getCount()) {
            if (!$CMCApiData = $this->api->getMapData()) {
                $this->consoleCommandLogger->notice('Tried to populate coinbase with data. No coin data or API is not available');
                $output->writeln(self::API_NOT_AVAILABLE_MESSAGE);

                return Command::FAILURE;
            }

            try {
                $coins = $this->service->deserializeArray($CMCApiData, 'App\\Entity\\Coin', '[data]');
                $coins = $this->removeObjectDuplicates($coins, 'abbreviation');

                foreach ($coins as $coin) {
                    $this->manager->persist($coin);
                }

                $this->manager->flush();
            } catch (\Throwable $t) {
                $this->consoleCommandLogger->error($t->getMessage().' '.$t->getFile().':'.$t->getLine());
                $output->writeln(self::FAIL_MESSAGE);

                return Command::FAILURE;
            }

            $output->writeln(self::SUCCESS_MESSAGE);

            return Command::SUCCESS;
        } else {
            $output->writeln(self::ALREADY_POPULATED_MESSAGE);

            return Command::FAILURE;
        }
    }
}

<?php

namespace App\Repository;

use App\Entity\BotChat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BotChat>
 *
 * @method BotChat|null find($id, $lockMode = null, $lockVersion = null)
 * @method BotChat|null findOneBy(array $criteria, array $orderBy = null)
 * @method BotChat[]    findAll()
 * @method BotChat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BotChatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BotChat::class);
    }

    public function save(BotChat $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BotChat $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return BotChat[] Returns an array of BotChat objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

    public function findOneByTelegramId(int $chatId): ?BotChat
    {
        return $this->createQueryBuilder('bc')
            ->andWhere('bc.ex_chat_id = :val')
            ->setParameter('val', $chatId)
            ->getQuery()
            ->getOneOrNullResult();
    }
}

<?php

namespace App\Repository;

use App\Entity\BotChatUpdate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BotChatUpdate>
 *
 * @method BotChatUpdate|null find($id, $lockMode = null, $lockVersion = null)
 * @method BotChatUpdate|null findOneBy(array $criteria, array $orderBy = null)
 * @method BotChatUpdate[]    findAll()
 * @method BotChatUpdate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BotChatUpdateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BotChatUpdate::class);
    }

    public function save(BotChatUpdate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BotChatUpdate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getLastRecord(): ?BotChatUpdate
    {
        return $this->createQueryBuilder('bcu')
            ->setMaxResults(1)
            ->orderBy('bcu.id', 'DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }

//    /**
//     * @return BotChatUpdate[] Returns an array of BotChatUpdate objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BotChatUpdate
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

<?php

namespace App\Repository;

use App\Entity\Coin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Coin>
 *
 * @method Coin|null find($id, $lockMode = null, $lockVersion = null)
 * @method Coin|null findOneBy(array $criteria, array $orderBy = null)
 * @method Coin[]    findAll()
 * @method Coin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Coin::class);
    }

    public function save(Coin $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Coin $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getCount()
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

//    /**
//     * @return Coin[] Returns an array of Coin objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

    public function findOneByAbbreviation(string $q): ?Coin
    {
        $q = trim($q);
        $abbreviation = strtoupper($q);

        return $this->createQueryBuilder('c')
            ->andWhere('c.abbreviation = :val')
            ->orWhere('c.abbreviation = :val2')
            ->setParameter('val', $abbreviation)
            ->setParameter('val2', $q)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByTitleOrAbbreviation(string $q): ?Coin
    {
        $q = trim($q);
        $title = ucfirst($q);
        $abbreviation = strtoupper($q);

        $foundExact = $this->createQueryBuilder('c')
            ->andWhere('c.title = :tval')
            ->orWhere('c.title = :tval2')
            ->orWhere('c.abbreviation = :aval')
            ->setParameter('tval', $q)
            ->setParameter('tval2', $title)
            ->setParameter('aval', $abbreviation)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($foundExact) {
            return $foundExact;
        }

        return $this->createQueryBuilder('c')
            ->andWhere('c.title like :val')
            ->setParameter('val', $title.'%')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}

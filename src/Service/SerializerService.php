<?php

namespace App\Service;

use App\Exception\ClassNotFoundException;
use App\Exception\JsonInvalidException;
use App\Exception\NoDataException;
use Doctrine\Common\Annotations\AnnotationReader;
use Monolog\Logger;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\UnwrappingDenormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class SerializerService
{
    private SerializerInterface $serializer;

    public function __construct()
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
        $encoders = [new JsonEncoder()];
        $normalizers = [
            new UnwrappingDenormalizer(),
            new ArrayDenormalizer(),
            new GetSetMethodNormalizer($classMetadataFactory, $metadataAwareNameConverter),
        ];
        $this->serializer = new Serializer($normalizers, $encoders);

        $this->logger = new Logger('serializer');
    }

    public function deserializeArray(string $data, string $objectClass, string $nestedPath = null): array
    {
        if (!is_array(json_decode($data, true))) {
            throw new JsonInvalidException();
        }

        if (!class_exists($objectClass)) {
            throw new ClassNotFoundException($objectClass);
        }

        if ($nestedPath) {
            try {
                return $this->serializer->deserialize($data, $objectClass.'[]', 'json', [UnwrappingDenormalizer::UNWRAP_PATH => $nestedPath]);
            } catch (\Throwable) {
                throw new NoDataException($nestedPath);
            }
        }

        return $this->serializer->deserialize($data, $objectClass.'[]', 'json');
    }

    public function denormalizeOne(array $data, string $objectClass): object
    {
        return $this->serializer->denormalize($data, $objectClass, 'json');
    }

    public function decode(string $data): array
    {
        return $this->serializer->decode($data, 'json');
    }
}

<?php

namespace App\Service;

class StartReplier implements ReplierInterface
{

    public function getReply(string $message = ''): string
    {
        return <<<EOT
Hello! I'm the bot, who shows you current prices and 24h change in % of some popular cryptocurrency.
Just type in any token title or symbol.
EOT;
    }
}
<?php

namespace App\Service;

interface ReplierInterface
{
    public function getReply(string $message = ''): string;
}
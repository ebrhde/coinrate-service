<?php

namespace App\Service;

use App\Enum\Trend;
use App\Repository\CoinRepository;

class RateReplier implements ReplierInterface
{
    public function __construct(private CoinRepository $coinRepository)
    {

    }


    public function getReply(string $message = ''): string
    {
        if ($coin = $this->coinRepository->findOneByTitleOrAbbreviation($message)) {
            $trendEmoji = '';

            switch ($coin->getTrend()) {
                case Trend::Up:
                    $trendEmoji = '↗ ';
                    break;
                case Trend::Down:
                    $trendEmoji = '↙ ';
                    break;
                default:
                    break;
            }

            return '<b>'.$coin->getAbbreviation().' | '.$coin->getTitle().'</b>'.PHP_EOL
                .'Price on UTC '.date('H:i d.m', $coin->getUpdated()->getTimestamp()).': '.PHP_EOL
                .$coin->getPrice().'$ ('.$trendEmoji.$coin->getDailyPriceChange().'%)';
        } else {
            return "Sorry, this token was not found in the bot's base.";
        }
    }
}
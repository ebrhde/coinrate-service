<?php

namespace App\Service;

use App\Api\TelegramApi;
use App\Entity\BotChat;
use App\Entity\BotChatUpdate;
use App\Exception\MessageNotSentException;
use App\Repository\BotChatRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HandleTelegramUpdateService
{
    private ReplierInterface $replier;

    public function __construct(
        private ContainerInterface $container,
        private BotChatRepository $chatRepository,
        private EntityManagerInterface $manager,
        private TelegramApi $api,
    ) {
    }

    public function handle(array $update): bool
    {
        $updateId = $update['update_id'];

        $sentAt = $update['message']['date'];
        $sentAtObj = new \DateTimeImmutable(date('Y-m-d H:i:s', $sentAt));

        $telegramChatId = $update['message']['chat']['id'];
        $telegramUsername = $update['message']['from']['username'];
        $message = $update['message']['text'];

        if (!$chat = $this->chatRepository->findOneByTelegramId($telegramChatId)) {
            $chat = new BotChat();
            $chat->setExChatId($telegramChatId);
            $chat->setTelegramUsername($telegramUsername);

            $this->manager->persist($chat);
        }

        $updateEntity = new BotChatUpdate();
        $updateEntity->setExUpdateId($updateId);
        $updateEntity->setChat($chat);
        $updateEntity->setExCreatedAt($sentAtObj);
        $updateEntity->setMessage($message);

        if(str_starts_with($message, '/')) {
            $replierName = ucfirst(ltrim($message, '/')) . 'Replier';

            $this->replier = $this->container->get('App\Service\\' . $replierName);

            $responseText = $this->replier->getReply();
        } else {
            $this->replier = $this->container->get('App\Service\RateReplier');

            $responseText = $this->replier->getReply($message);
        }

        $updateEntity->setResponse($responseText);

        if ($this->api->sendMessage($telegramChatId, $responseText)) {
            $this->manager->persist($updateEntity);
            $this->manager->flush();

            return true;
        } else {
            throw new MessageNotSentException($responseText);
        }
    }
}

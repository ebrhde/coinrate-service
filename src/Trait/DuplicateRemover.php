<?php

namespace App\Trait;

trait DuplicateRemover
{
    public function removeObjectDuplicates(array $input, string $keyName): array
    {
        $uniquesArr = [];

        $key = 'get' . ucfirst($keyName);

        foreach ($input as $item) {
            if(!isset($uniquesArr[$item->$key()])) {
                $uniquesArr[$item->$key()] = $item;
            }
        }

        return array_values($uniquesArr);
    }

    public function removeArrayDuplicates(array $input, string $keyName): array
    {
        $uniquesArr = [];

        foreach ($input as $item) {
            if(!isset($uniquesArr[$item[$keyName]])) {
                $uniquesArr[$item[$keyName]] = $item;
            }
        }

        return array_values($uniquesArr);
    }

}
<?php

namespace App\Api;

use Symfony\Contracts\HttpClient\HttpClientInterface;

interface CoinbaseApiInterface
{
    function __construct(string $apiKey, HttpClientInterface $client);

    function getListingData(): ?string;

    function getMapData(): ?string;
}
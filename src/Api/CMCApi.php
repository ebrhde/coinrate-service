<?php

namespace App\Api;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class CMCApi implements CoinbaseApiInterface
{
    private string $baseUrl = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/';

    public function __construct(private readonly string $apiKey, private readonly HttpClientInterface $client)
    {
    }

    /**
     * @return array<mixed>
     */
    public function getListingData(): ?string
    {
        $query = [
            'sort' => 'market_cap_strict',
            'limit' => 500,
        ];

        return $this->fetchData('listings/latest', $query);
    }

    public function getMapData(): ?string
    {
        $query = [
            'sort' => 'cmc_rank',
            'limit' => 500,
        ];

        return $this->fetchData('map', $query);
    }

    private function fetchData(string $endpoint, array $query): ?string
    {
        $response = $this->client->request(
            'GET',
            $this->baseUrl.$endpoint, [
                'headers' => [
                    'X-CMC_PRO_API_KEY' => $this->apiKey,
                    'Accept' => 'application/json',
                ],
                'query' => $query,
            ]
        );

        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];

        if (200 === $statusCode && 'application/json; charset=utf-8' === $contentType) {
            return $response->getContent();
        }

        return null;
    }
}

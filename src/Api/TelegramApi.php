<?php

namespace App\Api;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class TelegramApi
{
    private string $baseUrl;

    public function __construct(private readonly HttpClientInterface $client, string $botKey)
    {
        $this->baseUrl = 'https://api.telegram.org/bot'.$botKey.'/';
    }

    public function getUpdates(int $offset): ?array
    {
        $response = $this->client->request(
            'GET',
            $this->baseUrl.'getUpdates',
            [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'offset' => $offset,
                ],
            ]
        );

        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];
        $responseArray = $response->toArray();

        if (200 === $statusCode && 'application/json' === $contentType && $responseArray['ok']) {
            return $responseArray['result'];
        }

        return null;
    }

    public function sendMessage(int $chatId, string $text): bool
    {
        $response = $this->client->request(
            'POST',
            $this->baseUrl.'sendMessage',
            [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'body' => [
                    'parse_mode' => 'HTML',
                    'chat_id' => $chatId,
                    'text' => $text,
                ],
            ]
        );

        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];
        $responseArray = $response->toArray();

        if (200 === $statusCode && 'application/json' === $contentType && $responseArray['ok']) {
            return true;
        }

        return false;
    }
}

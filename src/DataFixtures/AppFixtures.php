<?php

namespace App\DataFixtures;

use App\Entity\Coin;
use App\Enum\Trend;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function __construct()
    {
    }

    public function load(ObjectManager $manager): void
    {
        $testData = [
            [
                'title' => 'Test Coin',
                'abbreviation' => 'TCN',
                'price' => '2.86',
                'daily_price_change' => '0.54',
                'trend' => Trend::Down,
            ],
            [
                'title' => 'Dummy Coin',
                'abbreviation' => 'DCN',
                'price' => '11.42',
                'daily_price_change' => '4.07',
                'trend' => Trend::Up,
            ],
        ];

        foreach ($testData as $testDatum) {
            $coin = new Coin();

            $coin->setTitle($testDatum['title']);
            $coin->setAbbreviation($testDatum['abbreviation']);
            $coin->setPrice($testDatum['price']);
            $coin->setDailyPriceChange($testDatum['daily_price_change']);
            $coin->setTrend($testDatum['trend']);

            $manager->persist($coin);
        }

        $manager->flush();
    }
}
